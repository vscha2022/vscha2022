FROM php:8-apache

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y apt-utils \
    && apt-get install -y sudo \
    && apt-get install -y git \
    && apt-get install -y less \
    && apt-get install -y zip unzip \
    && apt-get install -y mariadb-server \
    && /usr/local/bin/docker-php-ext-install mysqli

RUN service mariadb start \
    && mysql -u root -e "CREATE USER 'vscha2022'@'localhost' IDENTIFIED BY 'vscha2022'; GRANT ALL PRIVILEGES ON *.* TO 'vscha2022'@'localhost' IDENTIFIED BY 'vscha2022'; FLUSH PRIVILEGES;"\
    && mysql -u vscha2022 -pvscha2022 -e "CREATE DATABASE vscha2022; USE vscha2022; CREATE TABLE users (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(500) NOT NULL, year_of_birth INT NOT NULL, created DATETIME NOT NULL, updated DATETIME);"\
    && mkdir /var/www/html/vscha2022\
    && echo "SetEnv \"CHA_DB_HOST\" \"127.0.0.1\"\nSetEnv \"CHA_DB_USER\" \"vscha2022\"\nSetEnv \"CHA_DB_PASSWORD\" \"vscha2022\"\nSetEnv \"CHA_DB\" \"vscha2022\"" >> /var/www/html/vscha2022/.htaccess


ADD src /var/www/html/vscha2022/src/
COPY *.php /var/www/html/vscha2022/

EXPOSE 80

CMD service mariadb start && apache2ctl start && /bin/bash

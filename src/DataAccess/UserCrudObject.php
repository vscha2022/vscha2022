<?php

/**
 * CRUD object for UserRecord class.
 */
class UserCrudObject extends BaseCrudObject
{
    /**
     * Creates new instance of UserCrudObject.
     *
     * @param  mixed $conn Database connection.
     * @return void
     */
    public function __construct(mysqli $conn)
    {
        parent::__construct($conn, new UserCrudStatementMapper());
    }
}

?>

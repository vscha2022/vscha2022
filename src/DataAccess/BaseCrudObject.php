<?php

/**
 * Base class for CRUD database access objects.
 */
abstract class BaseCrudObject
{
    // TODO Transactions

    private mysqli $_conn;
    private BaseCrudStatementMapper $_mapper;
    
    /**
     * Creates new instance of BaseCrudObject.
     *
     * @param  mixed $conn Database connection.
     * @param  mixed $mapper CRUD statement mapper that will be used for mapping of objects to database and vice versa.
     * @return void
     */
    protected function __construct(mysqli $conn, BaseCrudStatementMapper $mapper)
    {
        $this->_conn = $conn;
        $this->_mapper = $mapper;
    }
    
    /**
     * Loads and object from database.
     *
     * @param  mixed $id Id of an object to load.
     * @return object Object loaded from the database. If object was not found null will be returned.
     */
    public function get(int $id): ?object
    {
        $stmt = $this->_conn->prepare($this->_mapper->get_get_sql());
        $this->_mapper->bind_get_params($stmt, $id);
        if(!$stmt_result = $stmt->execute())
        {
            return null;
        }

        $result = $this->_mapper->bind_get_result($stmt->get_result());
        $stmt->close();

        return $result;
    }
    
    /**
     * Creates new record in the database.
     *
     * @param  mixed $obj Object to be saved in database.
     * @return object On success returns clone of object that was passed to the method with id field set. Of failure returns null.
     */
    public function create(object $obj): ?object
    {
        $stmt = $this->_conn->prepare($this->_mapper->get_create_sql());
        $this->_mapper->bind_create_params($stmt, $obj);

        if(!$stmt_result = $stmt->execute() || $this->_conn->affected_rows == 1)
        {
            return null;
        }

        $result = $this->_mapper->bind_create_result($this->_conn->insert_id, $obj);
        $stmt->close();

        return $result;
    }
    
    /**
     * Updates database record.
     *
     * @param  mixed $obj Object to update in the database.
     * @return bool True on success, false on failure.
     */
    public function update(object $obj): bool
    {
        $stmt = $this->_conn->prepare($this->_mapper->get_update_sql());
        $this->_mapper->bind_update_params($stmt, $obj);
        $stmt_result = $stmt->execute() && $this->_conn->affected_rows == 1;
        $stmt->close();

        return $stmt_result;
    }
    
    /**
     * Deletes record from the database.
     *
     * @param  mixed $id Id of the object to delete.
     * @return bool True on success, false on failure.
     */
    public function delete(int $id): bool
    {
        $stmt = $this->_conn->prepare($this->_mapper->get_delete_sql());
        $this->_mapper->bind_delete_params($stmt, $id);
        $result = $stmt->execute() && $this->_conn->affected_rows == 1;
        $stmt->close();

        return $result;
    }
}

?>

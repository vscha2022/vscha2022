<?php

/**
 * Represents record in the users table.
 */
class UserRecord
{
    public int $id;
    public string $name;
    public int $year_of_birth;
    public DateTime $created;
    public ?DateTime $updated = null;
}

?>

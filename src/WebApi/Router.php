<?php

/**
 * Routes requests to appropriate handler. This implementation routes requests to single controller based on
 * HTTP method and request parameters.
 */
class Router
{
    private ICrudController $_controller;
    private IRequestMapper $_request_mapper;
    
    /**
     * Creates new instance of Router class.
     *
     * @param  mixed $controller Controller to route requests to.
     * @param  mixed $request_mapper Request mapper.
     * @return void
     */
    public function __construct(ICrudController $controller, IRequestMapper $request_mapper)
    {
        $this->_controller = $controller;
        $this->_request_mapper = $request_mapper;
    }
    
    /**
     * Dispatches request to aproriate handler and returns response object.
     *
     * @param  mixed $method HTTP method.
     * @return IResponse Response that will be sent back to the caller.
     */
    public function dispatch(string $method): IResponse
    {
        switch($method)
        {
            case "GET":
                $id = $this->_request_mapper->get_id();
                if ($id == null)
                {
                    return new BadRequestResponse("Parameter id not set.");
                }

                return $this->_controller->get($id);
                break;
            case "POST":
                $obj = $this->_request_mapper->get_object();
                if ($obj == null)
                {
                    return new BadRequestResponse("Object to create not provided.");
                }

                $id = $this->_request_mapper->get_id();

                // PHP does not support PUT requests out of the box ...
                if ($id == null)
                {
                    return $this->_controller->create($obj);
                }
                else
                {
                    return $this->_controller->update($id, $obj);
                }
                break;
            case "DELETE":
                $id = $this->_request_mapper->get_id();
                if ($id == null)
                {
                    return new BadRequestResponse("Parameter id not set.");
                }

                return $this->_controller->delete($id);
                break;
            default:
                return new MethodNotAllowedResponse("HTTP method ($method) not supported.");
        }
    }
}

?>

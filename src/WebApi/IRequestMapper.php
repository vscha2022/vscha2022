<?php

/**
 * Maps HTTP request parameters to user object used by a controller.
 */
interface IRequestMapper
{
    /**
     * Gets value of id parameter from the request.
     *
     * @return int Vale of id parameter or null if not present.
     */
    public function get_id(): ?int;

    /**
     * Maps request to appropriate object.
     *
     * @return UserRecord Object mapped from the request.
     */
    public function get_object(): ?object;
}

?>

<?php

/**
 * Maps HTTP request parameters to user object used by a controller.
 */
class UserRequestMapper implements IRequestMapper
{
    private array $_get;
    private array $_post;
    
    /**
     * Creates new instance of UserRequestMapper class.
     *
     * @param  mixed $get Get parameters array.
     * @param  mixed $post Post parameters array.
     * @return void
     */
    public function __construct($get, $post)
    {
        $this->_get = $get;
        $this->_post = $post;
    }

    /**
     * Gets value of id parameter from URL.
     *
     * @return int Vale of id parameter or null if not present.
     */
    public function get_id(): ?int
    {
        return $this->get_int_param($this->_get, "id");
    }
    
    /**
     * Maps request to user object.
     *
     * @return UserRecord User object mapped from the request.
     */
    public function get_object(): ?UserRecord
    {
        $user = new UserRecord();

        $user->id = $this->get_int_param($this->_get, "id") ?? -1;
        $user->year_of_birth = $this->get_int_param($this->_post, "year_of_birth") ?? -1;
        $user->name = $this->get_string_param($this->_post, "name") ?? "";

        return $user;
    }
    
    /**
     * Gets value of integer parameter.
     *
     * @param  mixed $params Associative array.
     * @param  mixed $param_name Name of the parameter to get.
     * @return int Value of the parameter or null if not present.
     */
    private function get_int_param(array $params, string $param_name): ?int
    {
        if(array_key_exists($param_name, $params) && $params[$param_name] && ctype_digit($params[$param_name]))
        {
            return intval($params[$param_name]);
        }

        return null;
    }
    
    /**
     * Gets value of string parameter from the request.
     *
     * @param  mixed $params Associative array.
     * @param  mixed $param_name Name of the parameter to get.
     * @return string Value of the parameter or null if not present.
     */
    private function get_string_param(array $params, string $param_name): ?string
    {
        if(array_key_exists($param_name, $params) && $params[$param_name])
        {
            return $params[$param_name];
        }

        return null;
    }
}

?>

<?php

/**
 * Handles CRUD requests.
 */
interface ICrudController
{    
    /**
     * Gets an object from the database based on the id provided.
     *
     * @param  mixed $id An id of the object.
     * @return IResponse A response object.
     */
    public function get(int $id): IResponse;
    
    /**
     * Creates new object and saves it to the database.
     *
     * @param  mixed $obj Object to create.
     * @return IResponse A response object.
     */
    public function create(object $obj): IResponse;
    
    /**
     * Updates an object and saves chanages to the database.
     *
     * @param  mixed $id An id of the object to update.
     * @param  mixed $obj Object to update.
     * @return IResponse A response object.
     */
    public function update(int $id, object $obj): IResponse;
    
    /**
     * Removes an object from the database.
     *
     * @param  mixed $id An id of the object to remove.
     * @return IResponse A response object.
     */
    public function delete(int $id): IResponse;
}

?>
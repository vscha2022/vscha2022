<?php

/**
 * Handles CRUD requests for user objects.
 */
class UserCrudController implements ICrudController
{
    private UserCrudObject $_crud;

    /**
     * Creates new instance of UserCrudController class.
     *
     * @param  mixed $crud Data access object for CRUD operations.
     * @return void
     */
    public function __construct(UserCrudObject $crud)
    {
        $this->_crud = $crud;
    }

    /**
     * Gets user object from database.
     *
     * @param  mixed $id User id.
     * @return IResponse A response object.
     */
    public function get(int $id): IResponse
    {
        $user = $this->_crud->get($id);

        if($user == null)
        {
            return new NotFoundResponse("User $id not found.");
        }

        return new OkResponse("This is $user->name, born in $user->year_of_birth.");
    }

    /**
     * Creates new user object and saves it to the database.
     *
     * @param  mixed $obj User to create.
     * @return IResponse A response object.
     */
    public function create(object $obj): IResponse
    {
        $validation_errors = $this->validate($obj);
        if(!empty($validation_errors))
        {
            return $this->send_validation_errors($validation_errors);
        }

        $obj->created = new DateTime();
        $user = $this->_crud->create($obj);

        if($user == null)
        {
            return new BadRequestResponse("Failed creating new user.");
        }

        return new CreatedResponse("New user with id '$user->id' has been created.");
    }

    /**
     * Update existing user object and saves changes to the database.
     *
     * @param  mixed $id Id of the user to update.
     * @param  mixed $obj Updated user object.
     * @return IResponse A response object.
     */
    public function update(int $id, object $obj): IResponse
    {
        $validation_errors = $this->validate($obj);
        if(!empty($validation_errors))
        {
            return $this->send_validation_errors($validation_errors);
        }

        $obj->updated = new DateTime();
        if(!$this->_crud->update($obj))
        {
            return new BadRequestResponse("Failed updating user with id '$id'.");
        }

        return new OkResponse("User with id '$id' has been updated.");
    }

    /**
     * Deletes user object from the database.
     *
     * @param  mixed $id An id of the object to delete.
     * @return IResponse A response object.
     */
    public function delete(int $id): IResponse
    {
        if(!$this->_crud->delete($id))
        {
            return new NotFoundResponse("User with id '$id' not found.");
        }

        return new OkResponse("User with id '$id' has been deleted.");
    }

    /**
     * Validates user object.
     *
     * @param  mixed $user Object to validate.
     * @return An array of validation errors.
     */
    private function validate(UserRecord $user)
    {
        $validation_errors = [];

        if(empty($user->name))
        {
            array_push($validation_errors, "Name is empty.");
        }

        if($user->year_of_birth < 0)
        {
            array_push($validation_errors, "Year of birth is negative.");
        }

        return $validation_errors;
    }

    /**
     * Sends validation errors to the client.
     *
     * @param  mixed $validation_errors Array of validation errors.
     * @return IResponse A response object.
     */
    private function send_validation_errors($validation_errors): IResponse
    {
        return new BadRequestResponse(implode("\n", $validation_errors));
    }
}

?>

<?php

/**
 * Interface for response objects. Controller methods return response object.
 * Response object set approriate HTTP status code and provide body of response.
 */
interface IResponse
{    
    /**
     * Sets HTTP status code and sends response body to the client.
     *
     * @return void
     */
    public function respond(): void;
}

/**
 * Base class for response objects.
 */
abstract class BaseResponse implements IResponse
{
    private string $_message;
    
    /**
     * Creates new instance of BaseResponse object.
     *
     * @param  mixed $message Message to send to the client.
     * @return void
     */
    public function __construct($message = "")
    {
        $this->_message = $message;
    }
    
    /**
     * Sets appropriate HTTP status code and sends respone back to the client.
     *
     * @return void
     */
    public function respond(): void
    {
        http_response_code($this->get_http_code());
        if($this->_message)
        {
            echo $this->_message;
        }
    }

    /**
     * Needs to be implemented by the derived class. Gets approriate HTTP code. 
     *
     * @return int HTTP code value.
     */
    protected abstract function get_http_code(): int;
}

/**
 * HTTP 400 Bad Request
 */
class BadRequestResponse extends BaseResponse
{
    public function get_http_code(): int
    {
        return 400;
    }
}

/**
 * HTTP 404 Not Found
 */
class NotFoundResponse extends BaseResponse
{
    public function get_http_code(): int
    {
        return 404;
    }
}

/**
 * HTTP 405 Method Not Allowed
 */
class MethodNotAllowedResponse extends BaseResponse
{
    public function get_http_code(): int
    {
        return 405;
    }
}

/**
 * HTTP 200 OK
 */
class OkResponse extends BaseResponse
{
    public function get_http_code(): int
    {
        return 200;
    }
}

/**
 * HTTP 201 Created
 */
class CreatedResponse extends BaseResponse
{
    public function get_http_code(): int
    {
        return 201;
    }
}

/**
 * HTTP 500 Internal Server Error
 */
class InternalServerErrorResponse extends BaseResponse
{
    public function get_http_code(): int
    {
        return 500;
    }
}

?>

# Technical Evaluation Exercise

## Project goal
Create proof of concept implementation of REST service for handling CRUD operations for an user entity. The code is not production ready, but it could serve as a proof of concept approach for implementation of a CRUD REST API.

## Code description
**[index.php](../index.php)** - First script that gets executed. Loads [error_handler.php](../error_handler.php) and [server.php](../server.php).

**[error_handler.php](../error_handler.php)** - Sets up global error handling routines so that server erros do not leak to the user.

**[server.php](../server.php)** - Performs:
1. Loading of scripts from [src](../src) folder.
2. Loading of database credentials from environment variables.
3. Opening of database connection.
4. Initialization of data access layer - [UserCrudObject](../src/DataAccess/UserCrudObject.php).
5. Initialization of request handling pipeline - [Router](../src/WebApi/Router.php), [UserCrudController](../src/WebApi/UserCrudController.php) and [UserRequestMapper](../src/WebApi/UserRequestMapper.php).
6. Error handling and cleanup.

**[src/DataAccess](../src/DataAccess)** - Data access wrapper around mysqli library which provides abstraction of database access to the rest of the application. Code in this folder is not covered by unit tests due to the limited amount of time for implementation of the project.

**[src/WebApi](../src/WebApi)** - Web API (i.e. REST API) layer. This code implements request handling logic in an extendable way and provides a simple framework for implementation of simple CRUD controllers. This code is covered by unit tests.

**[test](../test)** - Unit tests for [src/WebApi](../src/WebApi) code. Tests are based on [PHPUnit](https://phpunit.de/) framework.

**[Dockerfile](../Dockerfile)** - Docker file for building Docker image for running the project. Docker image contains everything needed for running the project code - PHP8, Apache2 and MariaDB. The Docker container produced by this Dockerfile can be used for demonstrational purposes only, i.e. it is not hardened, secured and it is not production ready.

## Instructions for building and running Docker container
1. Clone the repository.
2. Run docker build command.

        > docker build -t vscha2022 .
3. Start the container.

        > docker run -it -d -p 8080:80 vscha2022:latest
4. Send requests using your HTTP client of choice (e.g. curl).

        > curl -X POST -F 'name=Test User' -F 'year_of_birth=1990' http://localhost:8080/vscha2022/

        > curl -X GET http://localhost:8080/vscha2022/?id=1

        > curl -X POST -F 'name=Test User Updated' -F 'year_of_birth=1991' http://localhost:8080/vscha2022/?id=1

        > curl -X DELETE http://localhost:8080/vscha2022/?id=1

## Instructions for running unit tests
1. Install composer as described in [Composer Getting Started Guide](https://getcomposer.org/doc/00-intro.md).
2. Update dependencies.

        > php composer.phar update
3. Run unit tests.

        > ./vendor/bin/phpunit ./test/
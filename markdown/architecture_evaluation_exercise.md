# Architecture Evaluation Exercise
![Architecture diagram](./architecture_diagram.png)


The proposed architecture is based on AWS serverless architecture concepts. The serverless architecture offers easy scaling, simple management and fast prototyping. The architecture depicted in the diagram above relies heavily on AWS Lambda functions, message queues and other AWS serverless concepts. AWS is used as an example. Similar services are available from other Cloud providers.

Users, both internal (Article Editor) and external (User) interact with the application thru Single Page Application (SPA). Static assets of these SPAs are hosted in S3 buckets and server side logic is accessed through AWS API Gateway.

Data, as described in the requirements document is stored in RDBMS. In this case, serverless AWS Aurora RDBMS (MySql or Postgres).

Server side logic is implemented using AWS Lambda serverless stack. AWS Lambda functions are responsible for handling of various application events such as:

- publishing of new article
- sending notifications to the user
- managing alert definitions
- handling of bounced e-mail notifications

Components communicate asynchronously using AWS Simple Queue Service (SQS).

Scheduling of the timed notification process is controlled by AWS CloudWatch timer events.




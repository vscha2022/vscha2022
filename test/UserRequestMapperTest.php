<?php
use PHPUnit\Framework\TestCase;

class UserRequestMapperTest extends TestCase
{
    public function testGetIdReturnsNullWhenParameterNotSet(): void
    {
        $map = new UserRequestMapper([], []);

        $this->assertNull($map->get_id());
    }

    public function testGetIdReturnsValueWhenParameterSet(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), []);

        $this->assertEquals(1, $map->get_id());
    }

    public function testGetObjectReturnsCorrectType(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), array("year_of_birth"=>"2000", "name"=>"Test User"));

        $this->assertInstanceOf(UserRecord::class, $map->get_object());
    }

    public function testGetObjectSetsCorrectId(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), array("year_of_birth"=>"2000", "name"=>"Test User"));

        $this->assertEquals(1, $map->get_object()->id);
    }

    public function testGetObjectSetsDefaultIdWhenArgumentMissing(): void
    {
        $map = new UserRequestMapper([], array("year_of_birth"=>"2000", "name"=>"Test User"));

        $this->assertEquals(-1, $map->get_object()->id);
    }

    public function testGetObjectSetsCorrectYear(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), array("year_of_birth"=>"2000", "name"=>"Test User"));

        $this->assertEquals(2000, $map->get_object()->year_of_birth);
    }

    public function testGetObjectSetsDefaultYearWhenArgumentMissing(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), array("name"=>"Test User"));

        $this->assertEquals(-1, $map->get_object()->year_of_birth);
    }

    public function testGetObjectSetsCorrectName(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), array("year_of_birth"=>"2000", "name"=>"Test User"));

        $this->assertEquals("Test User", $map->get_object()->name);
    }

    public function testGetObjectSetsDefaultNameWhenArgumentMissing(): void
    {
        $map = new UserRequestMapper(array("id"=>"1"), array("year_of_birth"=>"2000"));

        $this->assertEquals("", $map->get_object()->name);
    }
}

?>

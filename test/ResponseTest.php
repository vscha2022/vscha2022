<?php
use PHPUnit\Framework\TestCase;

final class ResponseTest extends TestCase
{
    public function testBaseResponseDefaultMessageOutput(): void
    {
        $baseResponseMock = $this->mockBaseResponse(100);

        $this->expectOutputString("");

        $baseResponseMock->respond();
    }

    public function testBaseResponseMessageOutput(): void
    {
        $baseResponseMock = $this->mockBaseResponse(100, "XYZ");

        $this->expectOutputString("XYZ");

        $baseResponseMock->respond();
    }

    public function testBaseResponseCorrectResponseCodeSet(): void
    {
        $baseResponseMock = $this->mockBaseResponse(100);

        $baseResponseMock->respond();

        $this->assertEquals(100, http_response_code());
    }

    public function testBadRequestResponseReturnsCorrectCode(): void
    {
        $resp = new BadRequestResponse();
        $this->assertEquals(400, $resp->get_http_code());
    }

    public function testNotFoundResponseReturnsCorrectCode(): void
    {
        $resp = new NotFoundResponse();
        $this->assertEquals(404, $resp->get_http_code());
    }

    public function testMethodNotAllowedResponseReturnsCorrectCode(): void
    {
        $resp = new MethodNotAllowedResponse();
        $this->assertEquals(405, $resp->get_http_code());
    }

    public function testOkResponseResponseReturnsCorrectCode(): void
    {
        $resp = new OkResponse();
        $this->assertEquals(200, $resp->get_http_code());
    }

    public function testCreatedResponseReturnsCorrectCode(): void
    {
        $resp = new CreatedResponse();
        $this->assertEquals(201, $resp->get_http_code());
    }

    public function testInternalServerErrorResponseReturnsCorrectCode(): void
    {
        $resp = new InternalServerErrorResponse();
        $this->assertEquals(500, $resp->get_http_code());
    }

    private function mockBaseResponse(int $code, ?string $message = null)
    {
        $baseResponseMock = $message == null ?
                                $this->getMockForAbstractClass(BaseResponse::class, []) :
                                $this->getMockForAbstractClass(BaseResponse::class, [$message]);

        $baseResponseMock->method("get_http_code")
                         ->will($this->returnValue($code));

        return $baseResponseMock;
    }
}

?>
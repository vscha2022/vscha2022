<?php
use PHPUnit\Framework\TestCase;

class UserCrudControllerTest extends TestCase
{
    // Get method tests

    public function testGetReturnsNotFoundResponseWhenObjectNotFoundInDatabase(): void
    {
        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->assertInstanceOf(NotFoundResponse::class, $ctrl->get(1));
    }

    public function testGeCorrectMessageWhenObjectNotFoundInDatabase(): void
    {
        $id = 1;

        $this->expectOutputString("User $id not found.");

        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $ctrl->get($id)->respond();
    }

    public function testGetReturnsOkResponseWhenObjectFoundInDatabase(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject(getFunction: function() use($user) {
            return $user;
        }));

        $resp = $ctrl->get(1);

        $this->assertInstanceOf(OkResponse::class, $resp);
    }

    public function testGetCorrectMessageWhenObjectFoundInDatabase(): void
    {
        $user = $this->getDefaultUser();

        $this->expectOutputString("This is $user->name, born in $user->year_of_birth.");

        $ctrl = new UserCrudController($this->mockUserCrudObject(getFunction: function() use($user) {
            return $user;
        }));

        $ctrl->get(1)->respond();
    }

    // Create method tests

    public function testCreateReturnsBadRequestResponseWhenInsertFails(): void
    {
        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->assertInstanceOf(BadRequestResponse::class, $ctrl->create($this->getDefaultUser()));
    }

    public function testCreateCorrectMessageWhenInsertFails(): void
    {
        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->expectOutputString("Failed creating new user.");

        $ctrl->create($this->getDefaultUser())->respond();
    }

    public function testCreateReturnsCreatedResponseWhenInsertSucceeds(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject(createFunction: function() use($user) {
            return $user;
        }));

        $this->assertInstanceOf(CreatedResponse::class, $ctrl->create($user));
    }

    public function testCreateCorrectOutputWhenInsertSucceeds(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject(createFunction: function() use($user) {
            return $user;
        }));

        $this->expectOutputString("New user with id '$user->id' has been created.");

        $ctrl->create($user)->respond();
    }

    public function testCreateReturnsBadRequestResponseWhenYearNegative(): void
    {
        $user = $this->getDefaultUser();
        $user->year_of_birth = -1;

        $ctrl = new UserCrudController($this->mockUserCrudObject(createFunction: function() use($user) {
            return $user;
        }));

        $this->assertInstanceOf(BadRequestResponse::class, $ctrl->create($user));
    }

    public function testCreateReturnsBadRequestResponseWhenNameEmpty(): void
    {
        $user = $this->getDefaultUser();
        $user->name = "";

        $ctrl = new UserCrudController($this->mockUserCrudObject(createFunction: function() use($user) {
            return $user;
        }));

        $this->assertInstanceOf(BadRequestResponse::class, $ctrl->create($user));
    }

    // Update method tests

    public function testUpdateReturnsBadRequestResponseWhenUpdateFails(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->assertInstanceOf(BadRequestResponse::class, $ctrl->update($user->id, $user));
    }

    public function testUpdateCorrectMessageWhenUpdateFails(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->expectOutputString("Failed updating user with id '$user->id'.");

        $ctrl->update($user->id, $user)->respond();
    }

    public function testUpdateReturnsOkResponseWhenUpdateSucceeds(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject(updateFunction: function() {
            return true;
        }));

        $this->assertInstanceOf(OkResponse::class, $ctrl->update($user->id, $user));
    }

    public function testUpdateCorrectOutputWhenUpdateSucceeds(): void
    {
        $user = $this->getDefaultUser();

        $ctrl = new UserCrudController($this->mockUserCrudObject(updateFunction: function() {
            return true;
        }));

        $this->expectOutputString("User with id '$user->id' has been updated.");

        $ctrl->update($user->id, $user)->respond();
    }

    public function testUpdateReturnsBadRequestResponseWhenYearNegative(): void
    {
        $user = $this->getDefaultUser();
        $user->year_of_birth = -1;

        $ctrl = new UserCrudController($this->mockUserCrudObject(updateFunction: function() {
            return true;
        }));

        $this->assertInstanceOf(BadRequestResponse::class, $ctrl->update($user->id, $user));
    }

    public function testUpdateReturnsBadRequestResponseWhenNameEmpty(): void
    {
        $user = $this->getDefaultUser();
        $user->name = "";

        $ctrl = new UserCrudController($this->mockUserCrudObject(updateFunction: function() {
            return true;
        }));

        $this->assertInstanceOf(BadRequestResponse::class, $ctrl->update($user->id, $user));
    }

    // Delete method tests

    public function testDeleteReturnsNotFoundWhenDeleteFails(): void
    {
        $id = 1;

        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->assertInstanceOf(NotFoundResponse::class, $ctrl->delete($id));
    }

    public function testDeleteCorrectMessageWhenDeleteFails(): void
    {
        $id = 1;

        $ctrl = new UserCrudController($this->mockUserCrudObject());

        $this->expectOutputString("User with id '$id' not found.");

        $ctrl->delete($id)->respond();
    }

    public function testDeleteReturnsOkWhenDeleteSucceeds(): void
    {
        $id = 1;

        $ctrl = new UserCrudController($this->mockUserCrudObject(deleteFunction: function() {
            return true;
        }));

        $this->assertInstanceOf(OkResponse::class, $ctrl->delete($id));
    }

    public function testDeleteCorrectMessageWhenDeleteSucceeds(): void
    {
        $id = 1;

        $ctrl = new UserCrudController($this->mockUserCrudObject(deleteFunction: function() {
            return true;
        }));

        $this->expectOutputString("User with id '$id' has been deleted.");

        $ctrl->delete($id)->respond();
    }

    // Helpers

    private function mockUserCrudObject(?callable $getFunction = null, ?callable $createFunction = null,
        ?callable $updateFunction = null, ?callable $deleteFunction = null)
    {
        $mock = $this->createMock(UserCrudObject::class);

        $mock->method("get")
             ->willReturn($getFunction == null ? null : $getFunction());

        $mock->method("create")
             ->willReturn($createFunction == null ? null : $createFunction());

        $mock->method("update")
             ->willReturn($updateFunction == null ? false : $updateFunction());

        $mock->method("delete")
             ->willReturn($deleteFunction == null ? false : $deleteFunction());

        return $mock;
    }

    private function getDefaultUser(): UserRecord
    {
        $user = new UserRecord();
        $user->id = 1;
        $user->name = "Test User";
        $user->year_of_birth = 2000;
        $user->created = new DateTime();
        $user->updated = new DateTime();

        return $user;
    }
}

?>
<?php
use PHPUnit\Framework\TestCase;

final class RouterTest extends TestCase
{
    public function testDispatchUnsupportedMethod():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper());

        $rsp = $router->dispatch("PUT");

        $this->assertInstanceOf(MethodNotAllowedResponse::class, $rsp);
    }

    public function testDispatcGetNoIdReturnsBadRequest():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper());

        $rsp = $router->dispatch("GET");

        $this->assertInstanceOf(BadRequestResponse::class, $rsp);
    }

    public function testDispatcGetCallsGet():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper(1));

        $rsp = $router->dispatch("GET");

        $this->assertInstanceOf(OkResponse::class, $rsp);

        $this->expectOutputString("get"); // TODO Count how many times method got called instead of expecting output...

        $rsp->respond();
    }

    public function testDispatcPostNoObjectReturnsBadRequest():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper());

        $rsp = $router->dispatch("POST");

        $this->assertInstanceOf(BadRequestResponse::class, $rsp);
    }

    public function testDispatcPostNoIdCallsCreate():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper(null, new UserRecord()));

        $rsp = $router->dispatch("POST");

        $this->assertInstanceOf(OkResponse::class, $rsp);

        $this->expectOutputString("create");

        $rsp->respond();
    }

    public function testDispatcPostIdSetCallsCreate():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper(1, new UserRecord()));

        $rsp = $router->dispatch("POST");

        $this->assertInstanceOf(OkResponse::class, $rsp);

        $this->expectOutputString("update");

        $rsp->respond();
    }

    public function testDispatcDeleteNoIdReturnsBadRequest():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper());

        $rsp = $router->dispatch("DELETE");

        $this->assertInstanceOf(BadRequestResponse::class, $rsp);
    }

    public function testDispatcDeleteCallsGet():void
    {
        $router = new Router($this->mockCrudController(), $this->mockRequestMapper(1));

        $rsp = $router->dispatch("DELETE");

        $this->assertInstanceOf(OkResponse::class, $rsp);

        $this->expectOutputString("delete");

        $rsp->respond();
    }

    private function mockCrudController()
    {
        $mock = $this->createMock(ICrudController::class);

        $mock->method("get")
             ->willReturn(new OkResponse("get"));

        $mock->method("create")
             ->willReturn(new OkResponse("create"));

        $mock->method("update")
             ->willReturn(new OkResponse("update"));

        $mock->method("delete")
             ->willReturn(new OkResponse("delete"));

        return $mock;
    }

    private function mockRequestMapper(?int $id = 0, ?object $obj = null)
    {
        $mock = $this->createMock(IRequestMapper::class);

        $mock->method("get_id")
             ->willReturn($id);

        $mock->method("get_object")
             ->willReturn($obj);

        return $mock;
    }
}
?>
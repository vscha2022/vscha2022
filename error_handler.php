<?php

ini_set("display_errors", "Off"); 

function exceptions_error_handler($severity, $message, $filename, $lineno)
{
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
}

function shutdown_handler()
{
    $last_error = error_get_last();

    if(empty($last_error))
    {
        return;
    }

    switch ($last_error["type"])
    {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
            http_response_code(500);
            echo "Internal server error. Please contact administrator.";
    }
}

set_error_handler("exceptions_error_handler");
register_shutdown_function("shutdown_handler");
?>
<?php
$conn = null;

try
{
    require_once("./src/DataAccess/BaseCrudObject.php");
    require_once("./src/DataAccess/UserCrudObject.php");
    require_once("./src/DataAccess/BaseCrudStatementMapper.php");
    require_once("./src/DataAccess/UserCrudStatementMapper.php");
    require_once("./src/DataAccess/UserRecord.php");

    require_once("./src/WebApi/ICrudController.php");
    require_once("./src/WebApi/IRequestMapper.php");
    require_once("./src/WebApi/Response.php");
    require_once("./src/WebApi/Router.php");
    require_once("./src/WebApi/UserCrudController.php");
    require_once("./src/WebApi/UserRequestMapper.php");

    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    $getEnvVar = function(string $name, string $default = "") {
        if(($env = apache_getenv($name)) !== false)
        {
            return $env;
        }

        return $default;
    };
    
    $conn = new mysqli($getEnvVar("CHA_DB_HOST"),
                       $getEnvVar("CHA_DB_USER"),
                       $getEnvVar("CHA_DB_PASSWORD"),
                       $getEnvVar("CHA_DB"));

    $crud = new UserCrudObject($conn);
    $router = new Router(new UserCrudController($crud), new UserRequestMapper($_GET, $_POST));

    $router->dispatch($_SERVER["REQUEST_METHOD"])->respond();
}
catch(Throwable $ex)
{
    (new InternalServerErrorResponse("Internal server error. Please contact administrator."))->respond();
}
finally
{
    if($conn != null)
    {
        $conn->close();
    }
}

?>